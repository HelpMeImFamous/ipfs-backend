import localConfig from '../../localConfig'

var mailgun = require('mailgun-js')({
  apiKey: localConfig.mailgunKey,
  domain: localConfig.mailgunDomain
})

import express from 'express'

const { Router } = require('express')

const router = Router()

router.use(express.json())

router.post('/mailer', (req, res) => {
  let eto = req.body.eto
  let etext = req.body.etext
  let ehtml = req.body.ehtml
  var data = {
    from: 'IPFS uploader <noreply@pybossa.com>',
    to: eto,
    subject: 'New file shared with you',
    text: etext,
    html: ehtml
  }
  mailgun.messages().send(data, function(err, body) {
    return res.status(200).json(body)
  })
})
module.exports = router
