import express from 'express'
const { Router } = require('express')

const router = Router()

router.use(express.json())

var redis = require('redis')
var client = redis.createClient()

// if you'd like to select database 3, instead of 0 (default), call
// client.select(3, function() { /* ... */ });

client.on('error', function(err) {
  console.log('Error ' + err)
})

router.get('/links/:id', function(req, res, next) {
  let hash = decodeURIComponent(req.params.id)
  console.log('get', hash)
  client.get(hash, function(err, reply) {
    // reply is null when the key is missing
    res.json(reply)
  })
})

router.post('/links/new', (req, res) => {
  let hash = decodeURIComponent(req.body.hash)
  console.log('post', hash)
  let exp = parseInt(req.body.exp)
  let value = req.body.name
  if (client.set(hash, value, 'EX', exp)) {
    res.json({hash: req.body.hash, status: 'ok', name: value})
  } else {
    res.json({hash: req.body.hash, status: 'failed', name: value})
  }
})

module.exports = router
