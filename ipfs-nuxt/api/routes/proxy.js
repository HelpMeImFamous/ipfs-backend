import express from 'express'
const { Router } = require('express')

const router = Router()

router.use(express.json())

var redis = require('redis')
var client = redis.createClient()

import localConfig from '../../localConfig'
var CryptoJS = require('crypto-js')

var request = require('request')

// if you'd like to select database 3, instead of 0 (default), call
// client.select(3, function() { /* ... */ });

client.on('error', function(err) {
  console.log('Error ' + err)
})

router.get('/proxy/:id', function(req, res, next) {
  let encrypted = decodeURIComponent(req.params.id)
  let decrypted_hash = CryptoJS.AES.decrypt(encrypted, localConfig.secret)

  client.get(encrypted, function(err, reply) {
    if (reply) {
      let hash = decrypted_hash.toString(CryptoJS.enc.Utf8)
      let filelink = localConfig.ipfsBaseUrl + hash
      request(filelink).pipe(res)
    } else {
      res.sendStatus(404)
    }
  })
})

module.exports = router
