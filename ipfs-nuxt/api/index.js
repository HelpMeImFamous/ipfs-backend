import localConfig from '../localConfig.js'
import express from 'express'
const app = express()

// Require API routes
const links = require('./routes/links')
const mailer = require('./routes/mailer')
const proxy = require('./routes/proxy')

// Import API Routes
app.use(links)
app.use(mailer)
app.use(proxy)

// Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}

export default {
  path: '/api',
  handler: app
}
