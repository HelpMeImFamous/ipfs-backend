# ipfs-uploader

> IPFS uploader

You need to install Redis and get a Mailgun key to get it fully working. If you
are on Ubuntu, you can install redis like this:

```
apt-get install redis
```

For mailgun, go to their homepage.

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
