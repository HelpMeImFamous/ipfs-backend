IPFS backend
============

## Manual installation:

On production
```
sudo apt-get update
sudo apt-get install golang-go -y
curl -O https://dist.ipfs.io/go-ipfs/v0.4.18/go-ipfs_v0.4.18_linux-amd64.tar.gz
tar xvfz go-ipfs_v0.4.18_linux-amd64.tar.gz
sudo mv go-ipfs/ipfs /usr/local/bin/ipfs
```

Or use vagrant for testing
```
vagrant up
vagrant ssh
```

## Alternative setup IPFS manually (no vagrant setup):

```
ipfs init
ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/8080
ipfs config Addresses.API /ip4/0.0.0.0/tcp/5001
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["*"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["GET", "POST"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Headers '["Authorization"]'
ipfs config --json API.HTTPHeaders.Access-Control-Expose-Headers '["Location"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Credentials '["true"]'
```

## Start IPFS client

start the daemon

```
ipfs daemon
```

and

```
ipfs swarm peers
```

## IPFS web ui (does not work from Vagrant)

http://127.0.0.1:5001/webui

## Checking where the file has been stored

```
ipfs dht findprovs QHASH
```

## Getting the boostrap ID of your node

```
ipfs id --format="Peer ID: <id>\nPublic Key: <pubkey>"
```

## Setting a private network

Check the [official documentation](https://github.com/ipfs/go-ipfs/blob/master/docs/experimental-features.md#private-networks)


## Private network
Generate a pre-shared-key using ipfs-swarm-key-gen):

go get github.com/Kubuxu/go-ipfs-swarm-key-gen/ipfs-swarm-key-gen
ipfs-swarm-key-gen > ~/.ipfs/swarm.key
To join a given private network, get the key file from someone in the network and save it to ~/.ipfs/swarm.key (If you are using a custom $IPFS_PATH, put it in there instead).

When using this feature, you will not be able to connect to the default bootstrap nodes (Since we aren't part of your private network) so you will need to set up your own bootstrap nodes.

First, to prevent your node from even trying to connect to the default bootstrap nodes, run:

ipfs bootstrap rm --all
Then add your own bootstrap peers with:

ipfs bootstrap add <multiaddr>
For example:

ipfs bootstrap add /ip4/104.236.76.40/tcp/4001/ipfs/QmSoLV4Bbm51jM9C4gDYZQ9Cy3U6aXMJDAbzgu2fzaDs64
Bootstrap nodes are no different from all other nodes in the network apart from the function they serve.

To be extra cautious, You can also set the LIBP2P_FORCE_PNET environment variable to 1 to force the usage of private networks. If no private network is configured, the daemon will fail to start.


## Acknowledgements

Actividad subvencionada por el Ministerio de Cultura y Deporte del Gobierno de España con cargo a las Ayudas a la Modernización e Innovación en las Industrias Culturales y Creativas mediante proyectos digitales y tecnológicos, convocatoria 2018 

Project funded by the Spanish Ministry of Culture and Sports, thanks to the Aid to the Modernization and Innovation of Cultural and Creative Industrias by the means of digital and technological projects, 2018

![logo ministerio](http://www.helpmeimfamous.com/images/logotipobn.jpg)
